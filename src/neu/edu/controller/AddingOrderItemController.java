package neu.edu.controller;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.OrderItemRequest;
import neu.edu.controller.data.ProductRequest;
import neu.edu.controller.data.ResponseError;
import neu.edu.service.OrderItemService;
import neu.edu.service.ProductService;

@Path("/Customer/{custId}/Product/{prodId}")	
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class AddingOrderItemController {
	
	@Autowired
	private OrderItemService oiService;

	@POST
	@PermitAll
	@Path("/addOrderItem")
	
	public Response addOrderItem(@PathParam("prodId") String id,@PathParam("custId") String custId,OrderItemRequest oirequest) {
		boolean flag = false;
		try {
			flag = oiService.createOI(oirequest, Integer.parseInt(id),Integer.parseInt(custId));
		} catch (NumberFormatException ex) {
			return Response.ok().status(422).entity(new ResponseError("Invalid UserId Format")).build();
		}
		
		if(flag){
			return Response.ok().build();
		}else{
			return Response.ok().status(422).entity(new ResponseError("Resume Creation Failed")).build();

		}

	}
	
}
