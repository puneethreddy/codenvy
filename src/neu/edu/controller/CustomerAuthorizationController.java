package neu.edu.controller;

import javax.annotation.security.PermitAll;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context; 
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.AuthRequest;
import neu.edu.controller.data.AuthResponse;
import neu.edu.controller.data.ResponseError;
import neu.edu.controller.data.RestLogicalErrorException;
import neu.edu.controller.data.UserSession;
import neu.edu.service.AuthenticationService;

@Path("/auth")	
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerAuthorizationController {
	
	
		@Autowired
		private AuthenticationService authenticationService;
		
		
		@POST
		public Response authenticate(AuthRequest authRequest){
			
			UserSession userSession = null;
			try {
				userSession = authenticationService.validateUser(authRequest.getUsername(), authRequest.getPassword());
			} catch (RestLogicalErrorException e) {
				// TODO Auto-generated catch block
				return Response.ok().entity(e.getResponseError()).build();

			}
			return Response.ok().entity(userSession).build();
		}
									
			
//		@GET	
//		@Path("/username/password/role")
//		@PermitAll
//		public Response authenticate(@PathParam("username") String username,@PathParam("password") String password,@PathParam("role") String role) throws RestLogicalErrorException {
//			
//			AuthResponse authResponse=null;
//		
//		
//				authResponse=authenticationService.validateCustomer(username,password,role);				
//				
//			
//			
//			return Response.ok().entity(authResponse).build();
//			
//		}	
		
}
