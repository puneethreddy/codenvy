package neu.edu.controller.data;

public class OrderItemResponse {
	
	private Integer oiId;
	private Integer quantity;
	//private Integer orderId;
	//private Integer productId;
	
	public OrderItemResponse(){
		
	}
	
	public OrderItemResponse(int id,int qty){
		this.oiId=id;
		this.quantity=qty;
	}
	
	public Integer getOiId() {
		return oiId;
	}
	public void setOiId(Integer oiId) {
		this.oiId = oiId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	

//	public Integer getOrderId() {
//		return orderId;
//	}
//
//	public void setOrderId(Integer orderId) {
//		this.orderId = orderId;
//	}


	
}
