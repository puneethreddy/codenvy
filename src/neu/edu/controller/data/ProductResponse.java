package neu.edu.controller.data;

public class ProductResponse {

	private String productName;
	private String Description;
	private float Price;
	
	public ProductResponse(String prodName,String Desc,Float price){
		
		this.productName=prodName;
		this.Description=Desc;
		this.Price=price;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public float getPrice() {
		return Price;
	}
	public void setPrice(float price) {
		Price = price;
	}
	
	
	
}
