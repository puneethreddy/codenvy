package neu.edu.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.AuthResponse;
import neu.edu.controller.data.RestLogicalErrorException;
import neu.edu.controller.data.UserSession;
import neu.edu.dao.UserDao;
import neu.edu.mapping.Customer;
import neu.edu.mapping.User;
@Service
public class AuthenticationService {

	@Autowired
	private UserDao userDao;
	
	
//public AuthResponse validateCustomer(String username,String password, String role){
//	
//	User user= userDao.validateUser(username, password, role);
//	AuthResponse authResponse=new AuthResponse(user.getUserName(),user.getRole());
//	
//	return authResponse;
//}

public UserSession validateUser(String username,String password) throws RestLogicalErrorException{
	
	User user = userDao.validateUser(username, password);
	
	UserSession userSession = null;
	
	if(user != null){
		
		userSession = new  UserSession();
		//Customer customer=(Customer)user.getCustomers();
		//userSession.setId(String.valueOf(customer.getCustomerId()));
		userSession.setName(user.getFirstName());
		userSession.setRole(user.getRole());
		
	}else{
		RestLogicalErrorException authResponseErr = new RestLogicalErrorException("Invalid User");
		throw authResponseErr;
	}
	
	
	
	
	
	
	return userSession;
}
}
