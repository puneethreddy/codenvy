package neu.edu.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.ProductRequest;
import neu.edu.controller.data.ProductResponse;

import neu.edu.dao.ProductDao;

import neu.edu.mapping.Product;
import neu.edu.mapping.Seller;



@Service
public class ProductService {

	
		
		@Autowired
		private ProductDao productDao;
		
		
	public boolean createProduct(ProductRequest prodRequest,int sellerId){
		
		Product prod = new Product();
		Seller seller= new Seller();
		
		prod.setDescription(prodRequest.getDescription());
		prod.setPrice(prodRequest.getPrice());
		prod.setProductName(prodRequest.getProductName());
		
		seller.setSellerId(sellerId);
		prod.setSeller(seller);
		

		
		return productDao.createProduct(prod,sellerId);
		
	}
	
	
	public List<ProductResponse> getProductsList(){

		List<Product> prods = productDao.listProducts();
		List<ProductResponse> productResponse = null;
		if (prods != null) {
			//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			productResponse = new ArrayList<>();
			for (Product prod : prods) {
				ProductResponse response = new ProductResponse(prod.getProductName(),
															prod.getDescription(), 
															prod.getPrice());
														
				
				productResponse.add(response);
			}
		}
		return productResponse;
		
	}

}
