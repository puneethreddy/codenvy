package neu.edu.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.RegistrationRequest;
import neu.edu.controller.data.RestLogicalErrorException;
import neu.edu.dao.UserDao;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Seller;
import neu.edu.mapping.User;

@Service
public class RegistrationService {
	
	@Autowired
	private UserDao userDao;
	
	
	public boolean register(RegistrationRequest registrationRequest) throws RestLogicalErrorException{
		
		if(registrationRequest.getUsername() ==null ||
		   registrationRequest.getPassword() ==null){
			
			throw new RestLogicalErrorException("Registration Parameters incomplete.");
		}else{
			//Simulation a database Request
			Customer customer=new Customer();
			
		
			User user=new User();

			user.setFirstName(registrationRequest.getFirstName());
			user.setPassword(registrationRequest.getPassword());
			user.setUserName(registrationRequest.getUsername());
			user.setRole(registrationRequest.getRole());
			
			
			//customer.setCustomerId();
			customer.setUser(user);
		   user.getCustomers().add(customer);
		 //  user.setCustomers((Set<Customer>) customer);
			
//			user.setAddressLine1(registrationRequest.getAddressLine1());
//			user.setAddressLine2(registrationRequest.getAddressLine2());
//			user.setCity(registrationRequest.getCity());
			
			if(!userDao.registrationCustomer1(user,customer)){
					
//					registrationRequest.getUsername(),
//					registrationRequest.getPassword(),
//					registrationRequest.getFirstName(),
//					registrationRequest.getLastName(),
//					registrationRequest.getEmail(),
//					registrationRequest.getPhone(),
//					registrationRequest.getTitle(),
//					registrationRequest.getAddressLine1(),
//					registrationRequest.getAddressLine2(),
//					registrationRequest.getCity(),
//					registrationRequest.getState(),
//					registrationRequest.getZip(),
//					registrationRequest.getRole())){
				
				throw new RestLogicalErrorException("Duplicate User.");

			}
		}
		
		
		return true;
		
	}
	
	public boolean registerSeller(RegistrationRequest registrationRequest) throws RestLogicalErrorException{
		
		if(registrationRequest.getUsername() ==null ||
		   registrationRequest.getPassword() ==null){
			
			throw new RestLogicalErrorException("Registration Parameters incomplete.");
		}else{
			//Simulation a database Request
			Seller seller=new Seller();
			
		
			User user=new User();

			user.setFirstName(registrationRequest.getFirstName());
			user.setPassword(registrationRequest.getPassword());
			user.setUserName(registrationRequest.getUsername());
			user.setRole(registrationRequest.getRole());
			
			seller.setMerchandiseName(registrationRequest.getMerchandiseName());
			
			//customer.setCustomerId();
			seller.setUser(user);
		   user.getSellers().add(seller);
		 //  user.setCustomers((Set<Customer>) customer);
			
//			user.setAddressLine1(registrationRequest.getAddressLine1());
//			user.setAddressLine2(registrationRequest.getAddressLine2());
//			user.setCity(registrationRequest.getCity());
			
			if(!userDao.registrationSeller(user,seller)){
					
//					registrationRequest.getUsername(),
//					registrationRequest.getPassword(),
//					registrationRequest.getFirstName(),
//					registrationRequest.getLastName(),
//					registrationRequest.getEmail(),
//					registrationRequest.getPhone(),
//					registrationRequest.getTitle(),
//					registrationRequest.getAddressLine1(),
//					registrationRequest.getAddressLine2(),
//					registrationRequest.getCity(),
//					registrationRequest.getState(),
//					registrationRequest.getZip(),
//					registrationRequest.getRole())){
				
				throw new RestLogicalErrorException("Duplicate User.");

			}
		}
		
		
		return true;
		
	}
	
	

}
