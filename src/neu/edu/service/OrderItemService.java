package neu.edu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.OrderItemRequest;
import neu.edu.controller.data.OrderItemResponse;
import neu.edu.controller.data.ProductRequest;
import neu.edu.controller.data.ProductResponse;
import neu.edu.dao.OrderItemDao;
import neu.edu.dao.ProductDao;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Orderitem;
import neu.edu.mapping.Product;
import neu.edu.mapping.Seller;

@Service
public class OrderItemService {
	
	
	@Autowired
	private OrderItemDao oiDao;
	
	
	public boolean createOI(OrderItemRequest oi,int prodID,int custID){
	
	Product prod=new Product();
	prod.setProductId(prodID);
	
	Orderitem oiobj=new Orderitem();
	oiobj.setProduct(prod);
	oiobj.setQuantity(oi.getQuantity());
	
	Customer c=new Customer();
	c.setCustomerId(custID);
	oiobj.setCustomer(c);
	
	
	
//	Product prod = new Product();
//	Seller seller= new Seller();
//	
//	prod.setDescription(prodRequest.getDescription());
//	prod.setPrice(prodRequest.getPrice());
//	prod.setProductName(prodRequest.getProductName());
//	
//	seller.setSellerId(sellerId);
//	prod.setSeller(seller);
//	

	
	return oiDao.createOi(oiobj);
	
}
	
	public List<OrderItemResponse> getOiList(){

		List<Orderitem> ois = oiDao.listOrderItems();
		List<OrderItemResponse> oiResponse = null;
		if (ois != null) {
			//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			oiResponse = new ArrayList<>();
			for (Orderitem oi : ois) {
				OrderItemResponse response = new OrderItemResponse(oi.getOrderItemId(),
															oi.getQuantity()); 
															
														
				
				oiResponse.add(response);
			}
		}
		return oiResponse;
		
	}

}
