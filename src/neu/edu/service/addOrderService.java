package neu.edu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.OrderItemRequest;
import neu.edu.controller.data.OrderItemResponse;
import neu.edu.dao.OrderDao;
import neu.edu.dao.OrderItemDao;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Order;
import neu.edu.mapping.Orderitem;
import neu.edu.mapping.Product;
import neu.edu.model.OrderItemList;

@Service
public class addOrderService {

	@Autowired
	private OrderDao oDao;
	
	
//public Order placeOrder(int custID){
//	
//	
//	Order ord=new Order();
//	ord.setStatus("Order Placed");
//	//ord.setOrderedDate(orderedDate);
//	//oiobj.setQuantity(oi.getQuantity());
//	
//	Customer c=new Customer();
//	c.setCustomerId(custID);
//	ord.setCustomer(c);
//
////	Orderitem oi=new Orderitem();
////	oi.setOrder(ord);
//	
//	
////	Product prod = new Product();
////	Seller seller= new Seller();
////	
////	prod.setDescription(prodRequest.getDescription());
////	prod.setPrice(prodRequest.getPrice());
////	prod.setProductName(prodRequest.getProductName());
////	
////	seller.setSellerId(sellerId);
////	prod.setSeller(seller);
////	
//
//	
//	return oDao.placeOrder(ord);
//	
//}

public boolean setoi(List<OrderItemResponse> ol,String prodid,String custId){
	
	//OrderItemList oil=new OrderItemList();
	
	Order ord=new Order();
	ord.setStatus("Order Placed");
	//ord.setOrderedDate(orderedDate);
	//oiobj.setQuantity(oi.getQuantity());
	
	Customer c=new Customer();
	c.setCustomerId(Integer.parseInt(custId));
	ord.setCustomer(c);
	
	List<Orderitem> entityoilist = new ArrayList<>();
	
	for(OrderItemResponse oiresponse:ol){
		
		//oiresponse.setOrderId(o.getOrderId());
	
		Orderitem orditem=new Orderitem();
		
		orditem.setOrderItemId(oiresponse.getOiId());
		orditem.setQuantity(oiresponse.getQuantity());
		orditem.setOrder(ord);
		entityoilist.add(orditem);		
		//ol.getOis().add(orditem);
	
	}
	//for(Orderitem orditem:entityoilist){
		oDao.setoi(entityoilist,prodid,ord);
//	}
	
	//return oDao.setoi(o);
	return true;
}
	
}
