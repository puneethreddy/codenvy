package neu.edu.dao;

import java.util.List;
import java.util.Random;

import org.eclipse.jdt.internal.compiler.flow.FinallyFlowContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import neu.edu.mapping.Customer;
import neu.edu.mapping.Seller;
import neu.edu.mapping.User;



@Service
public class UserDao {
	
	@Autowired
	private SessionFactory sessionFactory;

//	private static Business business = ConfigureABusiness.getBusiness();
//
//	public User validateUser(String username, String password) {
//
//		for (User user : business.getUsers()) {
//			if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
//				return user;
//			}
//		}
//		return null;
//	}

//	public boolean registrationCustomer(String username, String password, String firstName, String lastName, String email, Integer phone,String title,String AddressLine1,String AddressLine2,String City, String State,Integer zip,String role) {
//
//		User userTemp = new User();
//		userTemp.setId(new Random().nextInt(100000));
//		userTemp.setPassword(password);
//		userTemp.setUsername(username);
//		userTemp.setRole("customer");
//
//		Person person = new Person();
//		person.setEmail(email);
//		person.setOccupationType(phone);
//		person.setTitle(title);
//		person.setFirstName(firstName);
//		person.setLastName(lastName);
//
//		userTemp.setPerson(person);
//
//		for (User user : business.getUsers()) {
//			if (user.getUsername().equals(userTemp.getUsername()) && user.getPassword().equals(userTemp.getPassword())) {
//				return false;
//			}
//		}
//
//		business.addUser(userTemp);
//
//		return true;
//	}
//	
	@Transactional
	public boolean registrationCustomer1(User user,Customer cust){
		Session session = sessionFactory.getCurrentSession();
					session.persist(user);
					session.persist(cust);
		
		return true;
		
	}
	
	@Transactional
	public boolean registrationSeller(User user,Seller seller){
		Session session = sessionFactory.getCurrentSession();
					session.persist(user);
					session.persist(seller);
		
		return true;
		
	}
	
	public User validateUser(String username, String password) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		
			
			Query query = session.createQuery("from User "
					+ "where userName=:un "
					+ "and password=:pass");
		query.setString("un", username);
		query.setString("pass", password);
	
		

		User userAccounts = (User) query.uniqueResult();
	session.close();	
			return userAccounts;
	
	}
//	
//	public User validateUser(String username, String password) {
//
//		for (User user : business.getUsers()) {
//			if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
//				return user;
//			}
//		}
//		return null;
//	}

}
