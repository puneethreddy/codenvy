package neu.edu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.mapping.Orderitem;
import neu.edu.mapping.Product;

@Service
public class OrderItemDao {

	

	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	@Transactional
	public boolean createOi(Orderitem oi){
	Session session = sessionFactory.getCurrentSession();
				
//	if(p.getSeller().getSellerId()==sellerId){
//		
//	}
				session.persist(oi);
					//session.persist(seller);
		
		return true;
		
	}
	
	public List<Orderitem> listOrderItems() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Orderitem");
		

		List<Orderitem> oi = (List<Orderitem>) query.list();
		
		try {
			session.close();
		} catch (Exception ex) {
			return null;
		}
		return oi;
	}
}
