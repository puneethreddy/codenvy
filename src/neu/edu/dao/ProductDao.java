package neu.edu.dao;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import neu.edu.mapping.Product;
import neu.edu.mapping.Seller;


@Service
public class ProductDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
//	
//	public boolean createResume(Resume resume,int userId) {
//		// TODO Auto-generated method stub
//		for(neu.edu.mapping.User user:business.getUsers()){
//			if(user.get == userId){
//				//Since Simulator ignoring checking duplicate Resume ID for user.
//				resume.setId(new Random().nextInt(100));
//				if(resume.getCreationDate() ==null){
//					resume.setCreationDate(new Date());
//				}
//				user.addResume(resume);
//				return true;
//			}
//		}
//		return false;
//	}
	
	@Transactional
	public boolean createProduct(Product p,int sellerId){
	Session session = sessionFactory.getCurrentSession();
				
//	if(p.getSeller().getSellerId()==sellerId){
//		
//	}
				session.persist(p);
					//session.persist(seller);
		
		return true;
		
	}

	public List<Product> listProducts() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Product");
		

		List<Product> prod = (List<Product>) query.list();
		
		try {
			session.close();
		} catch (Exception ex) {
			return null;
		}
		return prod;
	}
	
}
