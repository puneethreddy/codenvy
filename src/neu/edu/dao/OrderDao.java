package neu.edu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.mapping.Order;
import neu.edu.mapping.Orderitem;
import neu.edu.mapping.Product;
import neu.edu.model.OrderItemList;

@Service
public class OrderDao {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	
//	
//	@Transactional
//	public Order placeOrder(Order o){
//	Session session = sessionFactory.getCurrentSession();
//				
//	session.persist(o);		
//	//session.persist(oi);
//		
//		return o;
//		
//	}
	
	@Transactional
	public boolean setoi(List<Orderitem> oi,String prodid,Order o){
	Session session = sessionFactory.getCurrentSession();
	Query query = session.createQuery("from Product where productId=:prodid");
	query.setString("prodid", prodid);
	
	Product p=(Product)query.uniqueResult();
	session.save(o);
	for(Orderitem item: oi){
		item.setProduct(p);
		session.update(item);
	}
		
	//session.persist(oi);
		
		return true;
		
	}
}
