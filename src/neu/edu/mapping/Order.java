package neu.edu.mapping;
// Generated Dec 5, 2016 6:43:10 PM by Hibernate Tools 5.1.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Order generated by hbm2java
 */
public class Order implements java.io.Serializable {

	private Integer orderId;
	private Customer customer;
	private Date orderedDate;
	private String status;
	private Set<Orderitem> orderitems = new HashSet<Orderitem>(0);

	public Order() {
	}

	public Order(Customer customer) {
		this.customer = customer;
	}

	public Order(Customer customer, Date orderedDate, String status, Set<Orderitem> orderitems) {
		this.customer = customer;
		this.orderedDate = orderedDate;
		this.status = status;
		this.orderitems = orderitems;
	}

	public Integer getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getOrderedDate() {
		return this.orderedDate;
	}

	public void setOrderedDate(Date orderedDate) {
		this.orderedDate = orderedDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<Orderitem> getOrderitems() {
		return this.orderitems;
	}

	public void setOrderitems(Set<Orderitem> orderitems) {
		this.orderitems = orderitems;
	}

}
