package neu.edu.mapping;
// Generated Dec 5, 2016 6:43:10 PM by Hibernate Tools 5.1.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * User generated by hbm2java
 */
public class User implements java.io.Serializable {

	private String userName;
	private String password;
	private String role;
	private String firstName;
	private String lastName;
	private String title;
	private Date dateOfBirth;
	private Integer phone;
	private String email;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private Integer zip;
	private Set<Customer> customers = new HashSet<Customer>(0);
	private Set<Seller> sellers = new HashSet<Seller>(0);
	private Set<Admin> admins = new HashSet<Admin>(0);

	public User() {
	}

	public User(String userName, String password, String role, String firstName) {
		this.userName = userName;
		this.password = password;
		this.role = role;
		this.firstName = firstName;
	}

	public User(String userName, String password, String role, String firstName, String lastName, String title,
			Date dateOfBirth, Integer phone, String email, String addressLine1, String addressLine2, String city,
			String state, Integer zip, Set<Customer> customers, Set<Seller> sellers, Set<Admin> admins) {
		this.userName = userName;
		this.password = password;
		this.role = role;
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.dateOfBirth = dateOfBirth;
		this.phone = phone;
		this.email = email;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.customers = customers;
		this.sellers = sellers;
		this.admins = admins;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Integer getPhone() {
		return this.phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getZip() {
		return this.zip;
	}

	public void setZip(Integer zip) {
		this.zip = zip;
	}

	public Set<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	public Set<Seller> getSellers() {
		return this.sellers;
	}

	public void setSellers(Set<Seller> sellers) {
		this.sellers = sellers;
	}

	public Set<Admin> getAdmins() {
		return this.admins;
	}

	public void setAdmins(Set<Admin> admins) {
		this.admins = admins;
	}

}
